#!/bin/bash

# Variables
DBNAME=dbname


sudo apt-get install python-software-properties -y
sudo LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get install php7.0 php7.0-fpm php7.0-mysql php7.0-cli php7.0-curl php7.0-gd php7.0-intl php7.0-ldap php7.0-mbstring php7.0-xml php7.0-zip php7.0-pdo-sqlite -y

sudo apt-get --purge autoremove -y
sudo service php7.0-fpm restart

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server mysql-client
sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
sudo service mysql start

mysql -uroot -proot -e "CREATE DATABASE $DBNAME"
mysql -uroot -proot -e "CREATE DATABASE test_$DBNAME"
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'10.0.2.2' WITH GRANT OPTION"
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'ODBC'@'10.0.2.2' WITH GRANT OPTION"
sudo service mysql restart

sudo apt-get install nginx -y
sudo cat > /etc/nginx/sites-available/default <<- EOM
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /vagrant/webroot;
    index index.php index.html index.htm;

    server_name server_domain_or_IP;
	
	sendfile off;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(otf|eot|woff|ttf)$ {
        types     {font/opentype otf;}
        types     {application/vnd.ms-fontobject eot;}
        types     {font/truetype ttf;}
        types     {application/font-woff woff;}
        types     {application/font-woff2 woff2;}
    }
}
EOM
sudo service nginx restart

# install git
sudo apt-get -y install git

# install Composer
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
