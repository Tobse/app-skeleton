<?php
/* @var $this \Cake\View\View */
?>
<!doctype html>
<html class="no-js" lang="de">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <meta name="description" content="<?= $this->fetch('description') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= $this->fetch('meta') ?>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('styles.css') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->script('vendor/modernizr-2.8.3.min') ?>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">Sie benutzen einen <strong>veralteten</strong> Browser. Bitte <a
    href="http://browsehappy.com/">aktualisieren Sie Ihren Browser</a> um Ihr Weberlebnis zu verbessern.</p>
<![endif]-->

<header>
    
</header>
<section class="container" id="content">

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>


    <?= $this->fetch('content') ?>

</section>
<footer></footer>

<?= $this->Html->script('vendor/jquery-1.12.0.min'); ?>
<?= $this->Html->script('vendor/bootstrap.min'); ?>

<?= $this->fetch('script') ?>

<?= $this->Html->script('plugins'); ?>
<?= $this->Html->script('main'); ?>

<script>
    var gaProperty = 'UA-XXXXX-X';
    var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
        window[disableStr] = true;
    }
    function gaOptout() {
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
        window[disableStr] = true;
    }
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('set', 'anonymizeIp', true);ga('send','pageview');
</script>
</body>
</html>
